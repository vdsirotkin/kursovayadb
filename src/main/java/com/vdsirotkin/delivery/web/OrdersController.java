package com.vdsirotkin.delivery.web;

import com.vdsirotkin.delivery.db.BillEntity;
import com.vdsirotkin.delivery.db.DeliveryOrderEntity;
import com.vdsirotkin.delivery.db.RegionsEntity;
import com.vdsirotkin.delivery.db.dao.BillDAO;
import com.vdsirotkin.delivery.db.dao.OrderDAO;
import com.vdsirotkin.delivery.db.dao.RegionDAO;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by ������� on 22.11.2015.
 */
@Controller
@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')")
public class OrdersController {

    public static final Logger logger = LogManager.getLogger(OrdersController.class);

    @Autowired
    private OrderDAO orderDAO;

    @Autowired
    private RegionDAO regionDAO;

    @Autowired
    private BillDAO billDAO;

    @RequestMapping(path = {"/list.orders.form"}, method = RequestMethod.GET)
    public ModelAndView listOrders() {
        List<DeliveryOrderEntity> orders = orderDAO.listOrders();
        ModelAndView view = new ModelAndView("ordersList");
        view.addObject("orders", orders);
        return view;
    }

    @RequestMapping(path = "/done.orders.form", method = RequestMethod.GET)
    public ModelAndView markOrderAsDone(Integer orderId) {
        orderDAO.markOrderAsDone(orderId);
        return listOrders();
    }

    @RequestMapping(path = "/edit.orders.form", method = RequestMethod.GET)
    public ModelAndView editOrder(Integer orderId) {
        DeliveryOrderEntity order;
        if (orderId != -1) {
            order = orderDAO.getOrder(orderId);
        } else {
            order = new DeliveryOrderEntity();
        }
        List<RegionsEntity> regions = regionDAO.listRegions();
        List<Integer> billIds = billDAO.listBillNumbers();

        ModelAndView view = new ModelAndView("ordersEdit");
        view.addObject("order", order);
        view.addObject("regions", regions);
        view.addObject("billIds", billIds);
        return view;
    }

    @RequestMapping(path = "/edit.orders.form", method = RequestMethod.POST)
    public String editOrder(@ModelAttribute("order") DeliveryOrderEntity order, @RequestParam(value = "billIds") Integer[] billIds) {
        order.setOrderStatus(0);
        DeliveryOrderEntity mergedOrder = orderDAO.saveOrder(order);
        billDAO.saveBill(mergedOrder.getOrderId(), Arrays.asList(billIds));
        orderDAO.refreshEntity(mergedOrder);
        return "redirect:list.orders.form";
    }

    @RequestMapping(path = "/costcheck.orders.form", method = RequestMethod.GET)
    public ModelAndView checkCost(@ModelAttribute("order") DeliveryOrderEntity order, @RequestParam("billIds") Integer[] billIds) {
        ModelAndView view = new ModelAndView("ordersCostCheck");
        Double cost = 0.0;
        Integer volume = 0;
        RegionsEntity region = regionDAO.getRegionById(order.getRegionId());
        List<BillEntity> bills = billDAO.listBillsByIdString(billIds);
        for (BillEntity bill : bills) {
            volume += bill.getGoodCount() * bill.getGood().getGoodVolume();
        }
        cost = (double) ((double) region.getRegionCost() + ((double) region.getRegionCost() / 500.0) * volume);
        view.addObject("order", order);
        view.addObject("billIds", billIds);
        view.addObject("cost", cost);
        return view;
    }

    private Boolean billExists(List<Integer> bills, Integer billNumber) {
        for (Integer bill : bills) {
            if (bill.equals(billNumber)) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    @RequestMapping(path = "/checklist.orders.form", method = RequestMethod.GET)
    public ModelAndView checkList(@ModelAttribute("order") DeliveryOrderEntity order, String orderDate2, Integer billNumber) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date parse = format.parse(orderDate2);
            order.setOrderDate(parse);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ModelAndView view = new ModelAndView("ordersListCheck");
        List<BillEntity> bills = billDAO.listBillsByNumber(billNumber);
        List<Integer> billNumberList = billDAO.listBillNumbers();
        if (billExists(billNumberList, billNumber)) {
            view.addObject("bills", bills);
            view.addObject("order", order);
            view.addObject("billNumber", billNumber);
        } else {
            view.addObject("message", "Bill does not exist or already done!");
        }
        return view;
    }

    private boolean billDone(Integer billNumber) {
        List<Integer> billStatus = billDAO.getBillStatus(billNumber);
        return billStatus.equals(1);
    }

    @RequestMapping(path = "/remove.orders.form", method = RequestMethod.GET)
    public String removeOrder(Integer orderId) {
        orderDAO.removeOrder(orderId);
        return "redirect:list.orders.form";
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }

}
