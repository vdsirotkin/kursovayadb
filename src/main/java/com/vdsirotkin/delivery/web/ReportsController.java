package com.vdsirotkin.delivery.web;

import com.vdsirotkin.delivery.db.*;
import com.vdsirotkin.delivery.db.dao.CarReportDAO;
import com.vdsirotkin.delivery.db.dao.CarsDAO;
import com.vdsirotkin.delivery.db.dao.RegionReportDAO;
import com.vdsirotkin.delivery.db.dao.ReportsDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ������� on 06.12.2015.
 */
@Controller
@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_REPORT_VIEW', 'ROLE_REPORT_EDIT')")
public class ReportsController {

    @Autowired
    public ReportsDAO reportsDAO;

    @Autowired
    public RegionReportDAO regionReportDAO;

    @Autowired
    public CarsDAO carsDAO;

    @Autowired
    private CarReportDAO carReportDAO;

    @RequestMapping(path = "/list.reports.form")
    public ModelAndView listReports() {
        ModelAndView view = new ModelAndView("listReports");
        List<ReportsEntity> listReports = reportsDAO.listReports();
        for (ReportsEntity listReport : listReports) {
            listReport.setReportTypeString(ReportTypes.reportTypes.get(listReport.getReportType()));
        }
        view.addObject("listReports", listReports);
        return view;
    }

    @RequestMapping(path = "/delete.report.form")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_REPORT_EDIT')")
    public String deleteReport(Integer reportId) {
        reportsDAO.deleteReport(reportId);
        return "redirect:list.reports.form";
    }


    @RequestMapping(path = "/create.report.form", method = RequestMethod.GET)
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_REPORT_EDIT')")
    public String createReport() {
        return "editReport";
    }

    @RequestMapping(path = "/create.report.form", method = RequestMethod.POST)
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_REPORT_EDIT')")
    public ModelAndView saveReport(@ModelAttribute("report") ReportsEntity entity, String reportStartDate, String reportEndDate) {
        ModelAndView view = null;
        try {
            String message = reportsDAO.validateDate(entity);
            if (message == null) {
                Integer reportId = reportsDAO.saveReport(entity);
                switch (entity.getReportType()) {
                    case 1:
                        regionReportDAO.createReport(reportId, reportStartDate, reportEndDate);
                        break;
                    case 2:
                        carReportDAO.createReport(reportId, reportStartDate, reportEndDate);
                        break;
                }
                view = new ModelAndView("redirect:list.reports.form");
            } else {
                view = new ModelAndView("editReport");
                view.addObject("message", message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @RequestMapping(path = "/show.report.form", method = RequestMethod.GET)
    public ModelAndView showReport(Integer reportId, Integer reportType) {
        ModelAndView view = new ModelAndView("viewReport");
        List<List<String>> report = new ArrayList<List<String>>();
        switch (reportType) {
            case 1:
                List<RegionsReportEntity> regionsReportEntities = regionReportDAO.listRegionReportByReportId(reportId);
                for (RegionsReportEntity entity : regionsReportEntities) {
                    report.add(entity.toList());
                }
                break;
            case 2:
                List<CarReportEntity> carReportEntities = carReportDAO.listCarReportByReportId(reportId);
                for (CarReportEntity entity : carReportEntities) {
                    report.add(entity.toList());
                }
        }

        ReportsEntity entity = reportsDAO.getReport(reportId);
        String[] headers = ReportTypes.reportTypesHeaders.get(entity.getReportType());
        view.addObject("reportContent", report);
        view.addObject("headers", Arrays.asList(headers));
        return view;
    }

    @RequestMapping(path = "/break.everything.form")
    public ModelAndView breakEverything() {
        reportsDAO.deleteFromReport();
        return new ModelAndView("redirect:list.reports.form");
    }
}
