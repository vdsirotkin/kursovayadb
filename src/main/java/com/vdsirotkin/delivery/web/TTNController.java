package com.vdsirotkin.delivery.web;

import com.vdsirotkin.delivery.db.BillEntity;
import com.vdsirotkin.delivery.db.CarsEntity;
import com.vdsirotkin.delivery.db.DeliveryOrderEntity;
import com.vdsirotkin.delivery.db.dao.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by ������� on 15.12.2015.
 */
@Controller
public class TTNController {

    @Autowired
    private CarsDAO carsDAO;

    @Autowired
    private RegionDAO regionDAO;

    @Autowired
    private OrderDAO orderDAO;

    @Autowired
    private CarsDeliveryDAO carsDeliveryDAO;

    @Autowired
    private BillDAO billDAO;

    @RequestMapping(path = "/select.ttn.form")
    public ModelAndView selectTtnForm() {
        ModelAndView view = new ModelAndView("selectTtn");
        List<CarsEntity> cars = carsDAO.listCars();
        view.addObject("cars", cars);
        return view;
    }

    @RequestMapping(path = "/list.ttn.form")
    public ModelAndView listTtnForm(Integer carId, String time) {
        ModelAndView view = new ModelAndView("listTtn");
        List<Integer> orderIds = carsDeliveryDAO.getOrdersByIdAndTime(carId, time);
        view.addObject("orders", orderIds);
        return view;
    }

    @RequestMapping(path = "/print.ttn.form")
    public ModelAndView printTtnForm(Integer orderId) {
        ModelAndView view = new ModelAndView("printTtn");
        DeliveryOrderEntity order = orderDAO.getOrder(orderId);
        Integer billNumber = billDAO.getBillNumber(orderId);
        List<BillEntity> bills = billDAO.listBillsByOrderId(orderId);
        view.addObject("order", order);
        view.addObject("billNumber", billNumber);
        view.addObject("bills", bills);
        return view;
    }

}
