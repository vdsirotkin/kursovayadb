package com.vdsirotkin.delivery.web;

/**
 * Created by SBT-Sirotkin-VD on 17.11.2015.
 */

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {

    @RequestMapping( value = "/login", method = RequestMethod.GET )
    public String login() {
        return "login";
    }
}
