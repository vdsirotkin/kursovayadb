package com.vdsirotkin.delivery.web;

import com.vdsirotkin.delivery.db.CarsDeliveryEntity;
import com.vdsirotkin.delivery.db.CarsEntity;
import com.vdsirotkin.delivery.db.DeliveryOrderEntity;
import com.vdsirotkin.delivery.db.RegionsEntity;
import com.vdsirotkin.delivery.db.dao.CarsDAO;
import com.vdsirotkin.delivery.db.dao.CarsDeliveryDAO;
import com.vdsirotkin.delivery.db.dao.OrderDAO;
import com.vdsirotkin.delivery.db.dao.RegionDAO;
import com.vdsirotkin.delivery.util.CarSaveError;
import com.vdsirotkin.delivery.util.DateTimeHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by ������� on 07.12.2015.
 */
@Controller
@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_LOGISTIC')")
public class CarsOrdersController {

    @Autowired
    private CarsDAO carsDAO;

    @Autowired
    private RegionDAO regionDAO;

    @Autowired
    private OrderDAO orderDAO;

    @Autowired
    private CarsDeliveryDAO carsDeliveryDAO;

    @RequestMapping(path = "/list.carsorders.form", method = RequestMethod.GET)
    public ModelAndView listCarsOrders(@RequestParam(required = false) Integer regionId) {
        ModelAndView view = new ModelAndView("listCarsOrders");
        List<RegionsEntity> regions = regionDAO.listRegions();
        if (regionId != null) {
            carsDeliveryDAO.refreshAllEntities();
            orderDAO.refreshAllEntities();
            List<DeliveryOrderEntity> orders = orderDAO.listOrdersForTodayByRegion(regionId);
            List<CarsEntity> cars = carsDAO.listCarsByRegion(regionId);
            List<CarsDeliveryEntity> carsDeliveries = carsDeliveryDAO.listCarsDeliveries();
            List<CarSaveError> errors = carsDeliveryDAO.validate(regionId);
            view.addObject("regionId", regionId);
            view.addObject("cars", cars);
            view.addObject("orders", orders);
            view.addObject("carsDeliveries", carsDeliveries);
            view.addObject("errors", errors);
        }
        view.addObject("regions", regions);
        return view;
    }

    @RequestMapping(path = "/save.carorder.form", method = RequestMethod.POST)
    public ModelAndView saveCarOrderForm(Integer[] orderId, Integer[] carId, String[] orderTime, Integer regionId) {
        Timestamp[] dates = new Timestamp[orderTime.length];
        for (int i = 0; i < orderTime.length; i++) {
            String time = orderTime[i];
            dates[i] = DateTimeHelper.formatTimeString(time);
        }
        carsDeliveryDAO.saveCarDelivery(carId, orderId, dates);
        return listCarsOrders(regionId);
    }
}
