package com.vdsirotkin.delivery.web;

/**
 * Created by SBT-Sirotkin-VD on 17.11.2015.
 */
import com.vdsirotkin.delivery.db.DeliveryOrderEntity;
import com.vdsirotkin.delivery.db.dao.OrderDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class IndexController
{
    @Autowired
    private OrderDAO dao;

    @RequestMapping( value = "/", method = RequestMethod.GET )
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("index");
        return view;
    }
}