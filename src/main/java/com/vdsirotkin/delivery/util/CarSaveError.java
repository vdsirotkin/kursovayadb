package com.vdsirotkin.delivery.util;

/**
 * Created by ������� on 14.12.2015.
 */
public class CarSaveError {
    private Integer carId;
    private Integer carVolume;
    private String timeStr;
    private Integer takenVolume;

    public CarSaveError(Integer carId, Integer carVolume, String timeStr, Integer takenVolume) {
        this.carId = carId;
        this.carVolume = carVolume;
        this.timeStr = timeStr;
        this.takenVolume = takenVolume;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public Integer getCarVolume() {
        return carVolume;
    }

    public void setCarVolume(Integer carVolume) {
        this.carVolume = carVolume;
    }

    public String getTimeStr() {
        return timeStr;
    }

    public void setTimeStr(String timeStr) {
        this.timeStr = timeStr;
    }

    public Integer getTakenVolume() {
        return takenVolume;
    }

    public void setTakenVolume(Integer takenVolume) {
        this.takenVolume = takenVolume;
    }
}
