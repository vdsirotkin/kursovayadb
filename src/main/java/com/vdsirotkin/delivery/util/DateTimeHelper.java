package com.vdsirotkin.delivery.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by ������� on 15.12.2015.
 */
public class DateTimeHelper {

    public static final String FORMAT_STRING = "%sT%s";
    public static final SimpleDateFormat DATETIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public static Timestamp formatTimeString(String time) {
        String dateStr = String.format(FORMAT_STRING, DATE_FORMAT.format(new java.util.Date()), time);
        try {
            return new Timestamp(DATETIME_FORMAT.parse(dateStr).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


}
