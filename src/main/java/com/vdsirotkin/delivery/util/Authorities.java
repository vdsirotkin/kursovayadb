package com.vdsirotkin.delivery.util;

/**
 * Created by ������� on 21.12.2015.
 */
public enum Authorities {
    ROLE_ADMIN,
    ROLE_MANAGER,
    ROLE_LOGISTIC,
    ROLE_REPORT_VIEW,
    ROLE_REPORT_EDIT
}
