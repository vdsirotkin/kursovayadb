package com.vdsirotkin.delivery.db;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by ������� on 17.11.2015.
 */
@Entity
@Table(name = "BILL")
@NamedQueries({
        @NamedQuery(name = "findAllBills", query = "from BillEntity"),
        @NamedQuery(name = "findAllBillINumbers", query = "select billNumber from BillEntity group by billNumber"),
        @NamedQuery(name = "findAllBillsByNumber", query = "from BillEntity where billNumber=:billNumber and billStatus!=1"),
        @NamedQuery(name = "updateBill", query = "update BillEntity set orderId=:orderId where billId in (:billIds)"),
        @NamedQuery(name = "findBillsByIdString", query = "from BillEntity where billId in (:ids)")
})
public class BillEntity implements Serializable {
    private int billId;
    private Integer billNumber;
    private Integer goodCount;
    private Integer goodId;
    private Integer billStatus;
    private Integer orderId;
    private GoodsEntity good;

    @Id
    @Column(name = "BILL_ID")
    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    @Basic
    @Column(name = "BILL_NUMBER", updatable = false, insertable = false)
    public Integer getBillNumber() {
        return billNumber;
    }

    public void setBillNumber(Integer billNumber) {
        this.billNumber = billNumber;
    }

    @Basic
    @Column(name = "GOOD_COUNT")
    public Integer getGoodCount() {
        return goodCount;
    }

    public void setGoodCount(Integer goodCount) {
        this.goodCount = goodCount;
    }

    @Basic
    @Column(name = "BILL_STATUS")
    public Integer getBillStatus() {
        return billStatus;
    }

    @Basic
    @Column(name = "GOOD_ID")
    public Integer getGoodId() {
        return goodId;
    }

    public void setGoodId(Integer goodId) {
        this.goodId = goodId;
    }

    @Basic
    @Column(name = "ORDER_ID")
    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "GOOD_ID", referencedColumnName = "GOOD_ID", nullable = false, insertable = false, updatable = false)
    public GoodsEntity getGood() {
        return good;
    }

    public void setGood(GoodsEntity good) {
        this.good = good;
    }

    public void setBillStatus(Integer billStatus) {
        this.billStatus = billStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BillEntity that = (BillEntity) o;

        if (billId != that.billId) return false;
        if (billNumber != null ? !billNumber.equals(that.billNumber) : that.billNumber != null) return false;
        if (goodCount != null ? !goodCount.equals(that.goodCount) : that.goodCount != null) return false;
        if (goodId != null ? !goodId.equals(that.goodId) : that.goodId != null) return false;
        return !(billStatus != null ? !billStatus.equals(that.billStatus) : that.billStatus != null);

    }

    @Override
    public int hashCode() {
        int result = billId;
        result = 31 * result + (billNumber != null ? billNumber.hashCode() : 0);
        result = 31 * result + (goodCount != null ? goodCount.hashCode() : 0);
        result = 31 * result + (goodId != null ? goodId.hashCode() : 0);
        result = 31 * result + (billStatus != null ? billStatus.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BillEntity{" +
                "billId=" + billId +
                ", billNumber=" + billNumber +
                ", goodCount=" + goodCount +
                ", goodId=" + goodId +
                ", billStatus=" + billStatus +
                ", orderId=" + orderId +
                ", good=" + good +
                '}';
    }
}
