package com.vdsirotkin.delivery.db;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Виталий on 17.11.2015.
 */
@Entity
@Table(name = "DELIVERY_ORDER")
@NamedQueries(value = {
        @NamedQuery(name = "findAllOrders", query = "from DeliveryOrderEntity where orderStatus!=1"),
        @NamedQuery(name = "getOrder", query = "from DeliveryOrderEntity where orderId=:orderId"),
        @NamedQuery(name = "removeOrder", query = "delete from DeliveryOrderEntity where orderId=:orderId")
})
public class DeliveryOrderEntity implements Serializable{
    private int orderId;
    private Date orderDate;
    private String orderName;
    private String orderTelephone;
    private Integer orderStatus;
    private Integer regionId;
    private Set<BillEntity> listBills = new HashSet<BillEntity>();
    private RegionsEntity region;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ORDER_ID")
    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "ORDER_DATE")
    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    @Basic
    @Column(name = "ORDER_NAME")
    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    @Basic
    @Column(name = "ORDER_TELEPHONE")
    public String getOrderTelephone() {
        return orderTelephone;
    }

    public void setOrderTelephone(String orderTelephone) {
        this.orderTelephone = orderTelephone;
    }

    @Basic
    @Column(name = "REGION_ID")
    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionNumber) {
        this.regionId = regionNumber;
    }

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "ORDER_ID", referencedColumnName = "ORDER_ID")
    public Set<BillEntity> getListBills() {
        return listBills;
    }

    public void setListBills(Set<BillEntity> listBills) {
        this.listBills = listBills;
    }

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "REGION_ID", referencedColumnName = "REGION_ID", nullable = false, insertable = false, updatable = false)
    public RegionsEntity getRegion() {
        return region;
    }

    public void setRegion(RegionsEntity region) {
        this.region = region;
    }

    @Basic
    @Column(name = "ORDER_STATUS")
    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeliveryOrderEntity entity = (DeliveryOrderEntity) o;

        if (orderId != entity.orderId) return false;
        if (orderDate != null ? !orderDate.equals(entity.orderDate) : entity.orderDate != null) return false;
        if (orderName != null ? !orderName.equals(entity.orderName) : entity.orderName != null) return false;
        if (orderTelephone != null ? !orderTelephone.equals(entity.orderTelephone) : entity.orderTelephone != null)
            return false;
        return !(regionId != null ? !regionId.equals(entity.regionId) : entity.regionId != null);

    }

    @Override
    public int hashCode() {
        int result = orderId;
        result = 31 * result + (orderDate != null ? orderDate.hashCode() : 0);
        result = 31 * result + (orderName != null ? orderName.hashCode() : 0);
        result = 31 * result + (orderTelephone != null ? orderTelephone.hashCode() : 0);
        result = 31 * result + (regionId != null ? regionId.hashCode() : 0);
        return result;
    }


    @Override
    public String toString() {
        return "DeliveryOrderEntity{" +
                "orderId=" + orderId +
                ", orderDate=" + orderDate +
                ", orderName='" + orderName + '\'' +
                ", orderTelephone='" + orderTelephone + '\'' +
                ", regionId=" + regionId +
                ", listBills=" + listBills +
                ", region=" + region +
                '}';
    }
}