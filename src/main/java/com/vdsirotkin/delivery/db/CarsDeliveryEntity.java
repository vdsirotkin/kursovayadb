package com.vdsirotkin.delivery.db;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by sbt-sirotkin-vd on 09.12.2015.
 */
@Entity
@Table(name = "CARS_DELIVERY")
public class CarsDeliveryEntity {
    private int carDeliveryId;
    private Integer carId;
    private Integer orderId;
    private Timestamp deliveryTime;


    public CarsDeliveryEntity(Integer carId, Integer orderId, Timestamp deliveryTime) {
        this.carId = carId;
        this.orderId = orderId;
        this.deliveryTime = deliveryTime;
    }

    public CarsDeliveryEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CAR_DELIVERY_ID")
    public int getCarDeliveryId() {
        return carDeliveryId;
    }

    public void setCarDeliveryId(int carDeliveryId) {
        this.carDeliveryId = carDeliveryId;
    }

    @Basic
    @Column(name = "CAR_ID")
    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    @Basic
    @Column(name = "ORDER_ID")
    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer billId) {
        this.orderId = billId;
    }

    @Basic
    @Column(name = "DELIVERY_TIME")
    public Timestamp getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Timestamp deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CarsDeliveryEntity that = (CarsDeliveryEntity) o;

        if (carDeliveryId != that.carDeliveryId) return false;
        if (orderId != null ? !orderId.equals(that.orderId) : that.orderId != null) return false;
        if (carId != null ? !carId.equals(that.carId) : that.carId != null) return false;
        if (deliveryTime != null ? !deliveryTime.equals(that.deliveryTime) : that.deliveryTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = carDeliveryId;
        result = 31 * result + (carId != null ? carId.hashCode() : 0);
        result = 31 * result + (orderId != null ? orderId.hashCode() : 0);
        result = 31 * result + (deliveryTime != null ? deliveryTime.hashCode() : 0);
        return result;
    }
}
