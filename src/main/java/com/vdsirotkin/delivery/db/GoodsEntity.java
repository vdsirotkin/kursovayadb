package com.vdsirotkin.delivery.db;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

/**
 * Created by ������� on 17.11.2015.
 */
@Entity
@Table(name = "GOODS")
public class GoodsEntity {
    private BillEntity bill;
    private int goodId;
    private String goodName;
    private Integer goodVolume;

    @Id
    @Column(name = "GOOD_ID")
    public int getGoodId() {
        return goodId;
    }

    public void setGoodId(int goodId) {
        this.goodId = goodId;
    }

    @Basic
    @Column(name = "GOOD_NAME")
    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    @Basic
    @Column(name = "GOOD_VOLUME")
    public Integer getGoodVolume() {
        return goodVolume;
    }

    public void setGoodVolume(Integer goodVolume) {
        this.goodVolume = goodVolume;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GoodsEntity that = (GoodsEntity) o;

        if (goodId != that.goodId) return false;
        if (goodName != null ? !goodName.equals(that.goodName) : that.goodName != null) return false;
        if (goodVolume != null ? !goodVolume.equals(that.goodVolume) : that.goodVolume != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = goodId;
        result = 31 * result + (goodName != null ? goodName.hashCode() : 0);
        result = 31 * result + (goodVolume != null ? goodVolume.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "GoodsEntity{" +
                "goodId=" + goodId +
                ", goodName='" + goodName + '\'' +
                ", goodVolume=" + goodVolume +
                '}';
    }
}
