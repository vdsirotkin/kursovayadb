package com.vdsirotkin.delivery.db;

import javax.persistence.*;

/**
 * Created by sbt-sirotkin-vd on 09.12.2015.
 */
@Entity
@Table(name = "CARS")
public class CarsEntity {
    private int carId;
    private Integer carRegion;
    private Integer carVolume;
    private RegionsEntity region;

    @Id
    @Column(name = "CAR_ID")
    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    @Basic
    @Column(name = "CAR_REGION")
    public Integer getCarRegion() {
        return carRegion;
    }

    public void setCarRegion(Integer carRegion) {
        this.carRegion = carRegion;
    }

    @Basic
    @Column(name = "CAR_VOLUME")
    public Integer getCarVolume() {
        return carVolume;
    }

    public void setCarVolume(Integer carVolume) {
        this.carVolume = carVolume;
    }

    @ManyToOne
    @JoinColumn(name = "CAR_REGION", referencedColumnName = "REGION_ID", insertable = false, updatable = false)
    public RegionsEntity getRegion() {
        return region;
    }

    public void setRegion(RegionsEntity region) {
        this.region = region;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CarsEntity that = (CarsEntity) o;

        if (carId != that.carId) return false;
        if (carRegion != null ? !carRegion.equals(that.carRegion) : that.carRegion != null) return false;
        if (carVolume != null ? !carVolume.equals(that.carVolume) : that.carVolume != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = carId;
        result = 31 * result + (carRegion != null ? carRegion.hashCode() : 0);
        result = 31 * result + (carVolume != null ? carVolume.hashCode() : 0);
        return result;
    }
}
