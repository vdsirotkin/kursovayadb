package com.vdsirotkin.delivery.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Виталий on 06.12.2015.
 */
public interface ReportTypes {
    HashMap<Integer, String> reportTypes = new HashMap<Integer, String>(){{
        this.put(1, "Отчет об активности регионов");
        this.put(2, "Отчет об активности машин");
    }};

    HashMap<Integer, String[]> reportTypesHeaders = new HashMap<Integer, String[]>(){{
        this.put(1, new String[]{"Название региона", "Общее количество заказов", "Общий объем товаров", "Средняя цена заказа"});
        this.put(2, new String[]{"Номер машины", "Регион", "Количество заказов", "Средний объем"});
    }};
}
