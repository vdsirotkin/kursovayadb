package com.vdsirotkin.delivery.db;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ������� on 15.12.2015.
 */
@Entity
@Table(name = "car_report")
public class CarReportEntity {
    private int carReportRowId;
    private Integer carId;
    private String regionName;
    private Integer ordersCount;
    private Float volume;
    private Integer reportId;

    @Id
    @Column(name = "CAR_REPORT_ROW_ID")
    public int getCarReportRowId() {
        return carReportRowId;
    }

    public void setCarReportRowId(int carReportRowId) {
        this.carReportRowId = carReportRowId;
    }

    @Basic
    @Column(name = "CAR_ID")
    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    @Basic
    @Column(name = "REGION_NAME")
    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    @Basic
    @Column(name = "ORDERS_COUNT")
    public Integer getOrdersCount() {
        return ordersCount;
    }

    public void setOrdersCount(Integer ordersCount) {
        this.ordersCount = ordersCount;
    }

    @Basic
    @Column(name = "VOLUME")
    public Float getVolume() {
        return volume;
    }

    public void setVolume(Float volume) {
        this.volume = volume;
    }

    @Basic
    @Column(name = "REPORT_ID")
    public Integer getReportId() {
        return reportId;
    }

    public void setReportId(Integer reportId) {
        this.reportId = reportId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CarReportEntity that = (CarReportEntity) o;

        if (carReportRowId != that.carReportRowId) return false;
        if (carId != null ? !carId.equals(that.carId) : that.carId != null) return false;
        if (regionName != null ? !regionName.equals(that.regionName) : that.regionName != null) return false;
        if (ordersCount != null ? !ordersCount.equals(that.ordersCount) : that.ordersCount != null) return false;
        if (volume != null ? !volume.equals(that.volume) : that.volume != null) return false;
        if (reportId != null ? !reportId.equals(that.reportId) : that.reportId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = carReportRowId;
        result = 31 * result + (carId != null ? carId.hashCode() : 0);
        result = 31 * result + (regionName != null ? regionName.hashCode() : 0);
        result = 31 * result + (ordersCount != null ? ordersCount.hashCode() : 0);
        result = 31 * result + (volume != null ? volume.hashCode() : 0);
        result = 31 * result + (reportId != null ? reportId.hashCode() : 0);
        return result;
    }

    public List<String> toList() {
        List<String> output = new ArrayList<String>();
        output.add(carId.toString());
        output.add(regionName);
        output.add(ordersCount.toString());
        output.add(volume.toString());
        return output;
    }
}
