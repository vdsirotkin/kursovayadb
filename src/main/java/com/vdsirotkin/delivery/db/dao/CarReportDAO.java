package com.vdsirotkin.delivery.db.dao;

import com.vdsirotkin.delivery.db.CarReportEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by ������� on 15.12.2015.
 */
@Component
public class CarReportDAO extends AbstractDAO {

    @SuppressWarnings("unchecked")
    public List<CarReportEntity> listCarReportByReportId(Integer reportId) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        try {
            List resultList = entityManager.createQuery("from CarReportEntity where reportId=:id").setParameter("id", reportId).getResultList();
            return resultList;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
        return null;
    }

    public static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public void createReport(Integer reportId, String reportStartDate, String reportEndDate) throws ParseException {
        try {
            Connection connection = getConnectionForReport();
            CallableStatement call = connection.prepareCall("CALL SP_CREATE_CAR_REPORT(?,?,?)");
            call.setDate(1, new Date(FORMAT.parse(reportStartDate).getTime()));
            call.setDate(2, new Date(FORMAT.parse(reportEndDate).getTime()));
            call.setInt(3, reportId);
            call.executeQuery();
            connection.close();
            call.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
