package com.vdsirotkin.delivery.db.dao;

import com.vdsirotkin.delivery.util.Authorities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;

/**
 * Created by ������� on 21.12.2015.
 */
@Component
public class AbstractDAO {

    @Autowired
    private JpaTransactionManager transactionManagerAdmin;

    @Autowired
    private JpaTransactionManager transactionManagerLogistic;

    @Autowired
    private JpaTransactionManager transactionManagerManager;

    @Autowired
    private JpaTransactionManager transactionManagerReportView;

    @Autowired
    private JpaTransactionManager transactionManagerReportEdit;

    protected EntityManager getEntityManager() {
        Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        SimpleGrantedAuthority authority = (SimpleGrantedAuthority) authorities.toArray()[0];
        Authorities authorityEnum = Authorities.valueOf(authority.getAuthority());
        EntityManager entityManager;
        switch (authorityEnum) {
            case ROLE_ADMIN:
                entityManager = transactionManagerAdmin.getEntityManagerFactory().createEntityManager();
                break;
            case ROLE_MANAGER:
                entityManager = transactionManagerManager.getEntityManagerFactory().createEntityManager();
                break;
            case ROLE_LOGISTIC:
                entityManager = transactionManagerLogistic.getEntityManagerFactory().createEntityManager();
                break;
            case ROLE_REPORT_VIEW:
                entityManager = transactionManagerReportView.getEntityManagerFactory().createEntityManager();
                break;
            case ROLE_REPORT_EDIT:
                entityManager = transactionManagerReportEdit.getEntityManagerFactory().createEntityManager();
                break;
            default:
                entityManager = transactionManagerAdmin.getEntityManagerFactory().createEntityManager();
                break;
        }
        return entityManager;
    }

    protected Connection getConnectionForReport() throws SQLException {
        return transactionManagerAdmin.getDataSource().getConnection();
    }

}
