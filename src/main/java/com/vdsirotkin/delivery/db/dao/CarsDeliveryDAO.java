package com.vdsirotkin.delivery.db.dao;

import com.vdsirotkin.delivery.db.CarsDeliveryEntity;
import com.vdsirotkin.delivery.util.CarSaveError;
import com.vdsirotkin.delivery.util.DateTimeHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Виталий on 13.12.2015.
 */
@Component
public class CarsDeliveryDAO extends AbstractDAO {

    public static final String ERROR_MESSAGE = "Внимание! Машина №%s перегружена в %s! Доступный объем: %s, выбранный объем: %s\n";
    public static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("hh:mm");

    @SuppressWarnings("unchecked")
    public void saveCarDelivery(Integer[] carIds, Integer[] orderIds, Timestamp[] deliveryDates) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        try {
            if (carIds.length == orderIds.length) {
                Query updateQuery = entityManager.createQuery("update CarsDeliveryEntity set carId=:carId, deliveryTime=:deliveryTime where orderId=:orderId");
                List<Integer> handledOrders = entityManager.createQuery("select orderId from CarsDeliveryEntity group by orderId").getResultList();
                for (int i = 0; i < carIds.length; i++) {
                    Integer carId = carIds[i];
                    Integer orderId = orderIds[i];
                    Timestamp deliveryDate = deliveryDates[i];
                    if (!carId.equals(-1)) {
                        if (handledOrders.contains(orderId)) {
                            updateQuery.setParameter("carId", carId).setParameter("orderId", orderId).setParameter("deliveryTime", deliveryDate).executeUpdate();
                        } else {
                            CarsDeliveryEntity entity = new CarsDeliveryEntity(carId, orderId, deliveryDate);
                            entityManager.merge(entity);
                        }
                    }
                }
            } else {
                System.out.println("Cant save - different sizes");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
    }

    public void refreshAllEntities() {
        EntityManager entityManager = getEntityManager();
        List<CarsDeliveryEntity> entityList = listCarsDeliveries();
        for (CarsDeliveryEntity entity : entityList) {
            entityManager.refresh(entityManager.merge(entity));
        }
    }

    @SuppressWarnings("unchecked")
    public List<CarsDeliveryEntity> listCarsDeliveries() {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        try {
            List resultList = entityManager.createQuery("from CarsDeliveryEntity ").getResultList();
            return resultList;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<CarSaveError> validate(Integer regionId) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        List<CarSaveError> errorList = new ArrayList<CarSaveError>();
        try {
            List<Object[]> resultList = entityManager.createNativeQuery("SELECT c.CAR_ID, c.CAR_VOLUME, sum(g.GOOD_VOLUME*b.GOOD_COUNT), cd.DELIVERY_TIME FROM CARS_DELIVERY cd \n" +
                    "\tJOIN BILL b ON cd.ORDER_ID=b.ORDER_ID\n" +
                    "    JOIN GOODS g ON g.GOOD_ID=b.GOOD_ID\n" +
                    "    JOIN CARS c ON c.CAR_ID=cd.CAR_ID\n" +
                    "    WHERE YEAR(cd.DELIVERY_TIME) = YEAR(NOW()) AND MONTH(cd.DELIVERY_TIME) = MONTH(NOW()) AND DAY(cd.DELIVERY_TIME) = DAY(NOW()) AND c.CAR_REGION=:regionId" +
                    "    GROUP BY cd.CAR_ID, cd.DELIVERY_TIME;").setParameter("regionId", regionId).getResultList();
            for (Object[] objects : resultList) {
                Integer volume = (Integer) objects[1];
                Integer takenVolume = ((BigDecimal) objects[2]).intValue();
                if (takenVolume > volume) {
                    Timestamp date = (Timestamp) objects[3];
                    String dateStr = TIME_FORMAT.format(new java.util.Date(date.getTime()));
                    errorList.add(new CarSaveError(((Integer) objects[0]), volume, dateStr, takenVolume));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
        return errorList;
    }

    public List<Integer> getOrdersByIdAndTime(Integer carId, String time) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        Timestamp timestamp = DateTimeHelper.formatTimeString(time);
        try {
            List resultList = entityManager.createQuery("select orderId from CarsDeliveryEntity where carId=:carId and deliveryTime=:time")
                    .setParameter("carId", carId)
                    .setParameter("time", timestamp)
                    .getResultList();
            return resultList;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
        return null;
    }
}
