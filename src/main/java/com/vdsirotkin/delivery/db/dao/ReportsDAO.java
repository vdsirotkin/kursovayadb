package com.vdsirotkin.delivery.db.dao;

import com.vdsirotkin.delivery.db.ReportsEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by ������� on 06.12.2015.
 */
@Component
public class ReportsDAO extends AbstractDAO {

    public Integer saveReport(ReportsEntity report) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        try {
            Integer reportId = entityManager.merge(report).getReportId();
            return reportId;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
        return -1;
    }

    public String validateDate(ReportsEntity report) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        if (report.getReportStartDate().after(report.getReportEndDate())) {
            return "Начальная дата не может быть после конечной даты!";
        }
        try {
            List dates = entityManager.createQuery("select reportId, reportStartDate, reportEndDate from ReportsEntity where reportType=:reportType").setParameter("reportType", report.getReportType()).getResultList();
            for (Object date : dates) {
                Object[] datesArray = (Object[]) date;
                Integer reportId = (Integer) datesArray[0];
                Date startDate = ((Date) datesArray[1]);
                Date endDate = ((Date) datesArray[2]);
                if (report.getReportStartDate().after(startDate) && report.getReportStartDate().before(endDate)) {
                    return "Дата начала уже используется в другом отчете: "+ " <a href=\"/kurs/show.report.form?reportId=" + reportId + "&reportType=" + report.getReportType() + "\"\" class=\"btn btn-success\"> Просмотреть</a>";
                } else if (report.getReportEndDate().after(startDate) && report.getReportEndDate().before(endDate)) {
                    return "Конечная дата уже существует в другом отчете " + " <a href=\"/kurs/show.report.form?reportId=" + reportId + "&reportType=" + report.getReportType() + "\"\" class=\"btn btn-success\"> Просмотреть</a>";
                } else if (report.getReportStartDate().equals(startDate) && report.getReportEndDate().equals(endDate)) {
                    return "Данный отчет уже существует!" + " <a href=\"/kurs/show.report.form?reportId=" + reportId + "&reportType=" + report.getReportType() + "\"\" class=\"btn btn-success\"> Просмотреть</a>";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
        return null;
    }

    public List<ReportsEntity> listReports() {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        try {
            List<ReportsEntity> list = ((List<ReportsEntity>) entityManager.createQuery("from ReportsEntity ").getResultList());
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
        return null;
    }

    public void deleteReport(Integer reportId) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        try {
            entityManager.createQuery("delete from RegionsReportEntity where reportId=:reportId").setParameter("reportId", reportId).executeUpdate();
            entityManager.createQuery("delete from ReportsEntity where reportId=:reportId").setParameter("reportId", reportId).executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
    }

    public ReportsEntity getReport(Integer reportId) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        try {
            ReportsEntity entity = (ReportsEntity) entityManager.createQuery("from ReportsEntity where reportId=:reportId").setParameter("reportId", reportId).getSingleResult();
            return entity;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
        return null;
    }

    public void deleteFromReport() {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        try {
            entityManager.createQuery("delete from ReportsEntity where reportId=-1").executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
    }
}
