package com.vdsirotkin.delivery.db.dao;

import com.vdsirotkin.delivery.db.BillEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ������� on 17.11.2015.
 */
@Component
public class BillDAO extends AbstractDAO {

    public List<BillEntity> listBill() {
        EntityManager entityManager = getEntityManager();
        List findAllBills = null;
        try {
            findAllBills = entityManager.createNamedQuery("findAllBills").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
        return findAllBills;
    }

    public List<Integer> listBillNumbers() {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        List ids = null;
        try {
            ids = entityManager.createNamedQuery("findAllBillINumbers").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
        return ids;
    }

    public List<BillEntity> listBillsByNumber(Integer billNumber) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        List bills = new ArrayList();
        try {
            bills = entityManager.createNamedQuery("findAllBillsByNumber").setParameter("billNumber", billNumber).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
        return bills;
    }

    public void saveBill(int orderId, List<Integer> billIds) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        try {
            entityManager.createNamedQuery("updateBill").setParameter("billIds", billIds).setParameter("orderId", orderId).executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
    }

    public List<BillEntity> listBillsByIdString(Integer[] billIds) {
        List<BillEntity> entityList = new ArrayList<BillEntity>();
        List<Integer> idsList = Arrays.asList(billIds);
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        try {
            entityList = ((List<BillEntity>) entityManager.createNamedQuery("findBillsByIdString").setParameter("ids", idsList).getResultList());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
        return entityList;
    }

    public List<Integer> getBillStatus(Integer billNumber) {
        EntityManager manager = getEntityManager();
        manager.getTransaction().begin();
        try {
            List resultList = manager.createQuery("select billStatus from BillEntity where billNumber=:billNumber").setParameter("billNumber", billNumber).getResultList();
            return resultList;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            manager.getTransaction().commit();
        }
        return null;
    }

    public Integer getBillNumber(Integer orderId) {
        EntityManager manager = getEntityManager();
        manager.getTransaction().begin();
        try {
            Integer number = (Integer) manager.createQuery("select billNumber from BillEntity where orderId=:orderId group by orderId").setParameter("orderId", orderId).getSingleResult();
            return number;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            manager.getTransaction().commit();
        }
        return null;
    }

    public List<BillEntity> listBillsByOrderId(Integer orderId) {
        EntityManager manager = getEntityManager();
        manager.getTransaction().begin();
        try {
            List bills = manager.createQuery("from BillEntity where orderId=:orderId").setParameter("orderId", orderId).getResultList();
            return bills;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            manager.getTransaction().commit();
        }
        return null;
    }
}
