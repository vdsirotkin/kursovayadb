package com.vdsirotkin.delivery.db.dao;

import com.vdsirotkin.delivery.db.CarsEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by sbt-sirotkin-vd on 09.12.2015.
 */
@Component
public class CarsDAO extends AbstractDAO {

    public List<CarsEntity> listCars() {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        try {
            List resultList = entityManager.createQuery("from CarsEntity ").getResultList();
            return resultList;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
        return null;
    }

    public List<CarsEntity> listCarsByRegion(Integer regionId) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        try {
            List<CarsEntity> resultList = (List<CarsEntity>) entityManager.createQuery("from CarsEntity where carRegion=:regionId").setParameter("regionId", regionId).getResultList();
            return resultList;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
        return null;
    }

}
