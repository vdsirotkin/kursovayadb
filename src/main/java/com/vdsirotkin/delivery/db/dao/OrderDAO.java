package com.vdsirotkin.delivery.db.dao;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import com.vdsirotkin.delivery.db.BillEntity;
import com.vdsirotkin.delivery.db.DeliveryOrderEntity;
import com.vdsirotkin.delivery.util.Authorities;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.swing.text.html.parser.Entity;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by ������� on 18.11.2015.
 */
@Component
public class OrderDAO extends AbstractDAO{
    
    public List<DeliveryOrderEntity> listOrders() {
        List findAllOrders = new ArrayList();
        EntityManager entityManager = getEntityManager();
        try {
            entityManager.getTransaction().begin();
            findAllOrders = entityManager.createNamedQuery("findAllOrders").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
        System.out.println(findAllOrders);
        return findAllOrders;
    }

    public DeliveryOrderEntity getOrder(Integer orderId) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        Object singleResult = entityManager.createNamedQuery("getOrder").setParameter("orderId", orderId).getSingleResult();
        entityManager.getTransaction().commit();
        return ((DeliveryOrderEntity) singleResult);
    }

    public DeliveryOrderEntity saveOrder(DeliveryOrderEntity order) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        try {
            return entityManager.merge(order);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
        return null;
    }

    public void refreshEntity(DeliveryOrderEntity order) {
        EntityManager entityManager = getEntityManager();
        entityManager.refresh(entityManager.merge(order));
    }

    public void refreshAllEntities() {
        List<DeliveryOrderEntity> entities = listOrders();
        for (DeliveryOrderEntity entity : entities) {
            refreshEntity(entity);
        }
    }

    public void removeOrder(Integer orderId) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        try {
            entityManager.createQuery("delete from CarsDeliveryEntity where orderId=:orderId").setParameter("orderId", orderId).executeUpdate();
            entityManager.createQuery("update BillEntity set orderId=null where orderId=:orderId").setParameter("orderId", orderId).executeUpdate();
            entityManager.createNamedQuery("removeOrder").setParameter("orderId", orderId).executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
    }

    @SuppressWarnings("unchecked")
    public List<DeliveryOrderEntity> listOrdersForTodayByRegion(Integer regionId) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        try {
            Query query = entityManager.createQuery("from DeliveryOrderEntity where regionId=:regionId and orderDate <= current_date and orderStatus!=1");
            List<DeliveryOrderEntity> resultList = (List<DeliveryOrderEntity>) query.setParameter("regionId", regionId).getResultList();
            return resultList;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
        return null;
    }

    public void markOrderAsDone(Integer orderId) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        try {
            entityManager.createQuery("update DeliveryOrderEntity set orderStatus=1 where orderId=:orderId").setParameter("orderId", orderId).executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
    }
}
