package com.vdsirotkin.delivery.db.dao;

import com.vdsirotkin.delivery.db.RegionsEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by ������� on 22.11.2015.
 */
@Component
public class RegionDAO extends AbstractDAO {

    public List<RegionsEntity> listRegions() {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        List findAllRegions = null;
        try {
            findAllRegions = entityManager.createNamedQuery("findAllRegions").getResultList();
            return findAllRegions;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
        return null;
    }

    public RegionsEntity getRegionById(Integer regionId) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();
        RegionsEntity region = new RegionsEntity();
        try {
            region = (RegionsEntity) entityManager.createNamedQuery("findRegionById").setParameter("regionId", regionId).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.getTransaction().commit();
        }
        return region;
    }

}
