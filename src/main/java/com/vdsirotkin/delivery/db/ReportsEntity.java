package com.vdsirotkin.delivery.db;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.sql.Date;

/**
 * Created by ������� on 06.12.2015.
 */
@Entity
@Table(name = "REPORTS")
public class ReportsEntity {
    private int reportId;
    private String reportName;
    private Integer reportType;
    private String reportTypeString;
    private Date reportStartDate;
    private Date reportEndDate;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "REPORT_ID")
    public int getReportId() {
        return reportId;
    }

    public void setReportId(int reportId) {
        this.reportId = reportId;
    }

    @Basic
    @Column(name = "REPORT_NAME")
    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    @Basic
    @Column(name = "REPORT_TYPE")
    public Integer getReportType() {
        return reportType;
    }

    public void setReportType(Integer reportType) {
        this.reportType = reportType;
    }

    @Transient
    public String getReportTypeString() {
        return reportTypeString;
    }

    public void setReportTypeString(String reportTypeString) {
        this.reportTypeString = reportTypeString;
    }

    @Basic
    @Column(name = "REPORT_START_DATE")
    public Date getReportStartDate() {
        return reportStartDate;
    }

    public void setReportStartDate(Date reportStartDate) {
        this.reportStartDate = reportStartDate;
    }

    @Basic
    @Column(name = "REPORT_END_DATE")
    public Date getReportEndDate() {
        return reportEndDate;
    }

    public void setReportEndDate(Date reportEndDate) {
        this.reportEndDate = reportEndDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReportsEntity that = (ReportsEntity) o;

        if (reportId != that.reportId) return false;
        if (reportName != null ? !reportName.equals(that.reportName) : that.reportName != null) return false;
        return !(reportType != null ? !reportType.equals(that.reportType) : that.reportType != null);

    }

    @Override
    public int hashCode() {
        int result = reportId;
        result = 31 * result + (reportName != null ? reportName.hashCode() : 0);
        result = 31 * result + (reportType != null ? reportType.hashCode() : 0);
        return result;
    }
}
