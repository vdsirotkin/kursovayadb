package com.vdsirotkin.delivery.db;

import javax.persistence.*;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by ������� on 06.12.2015.
 */
@Entity
@Table(name = "REGIONS_REPORT")
public class RegionsReportEntity {
    private int reportRowId;
    private String regionName;
    private Integer overallCount;
    private Integer overallVolume;
    private Float averageCost;
    private Integer reportId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "REPORT_ROW_ID")
    public int getReportRowId() {
        return reportRowId;
    }

    public void setReportRowId(int reportRowId) {
        this.reportRowId = reportRowId;
    }

    @Basic
    @Column(name = "REGION_NAME")
    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    @Basic
    @Column(name = "OVERALL_COUNT")
    public Integer getOverallCount() {
        return overallCount;
    }

    public void setOverallCount(Integer overallCount) {
        this.overallCount = overallCount;
    }

    @Basic
    @Column(name = "OVERALL_VOLUME")
    public Integer getOverallVolume() {
        return overallVolume;
    }

    public void setOverallVolume(Integer overallVolume) {
        this.overallVolume = overallVolume;
    }

    @Basic
    @Column(name = "AVERAGE_COST")
    public Float getAverageCost() {
        return averageCost;
    }

    public void setAverageCost(Float averageCost) {
        this.averageCost = averageCost;
    }

    @Basic
    @Column(name = "REPORT_ID")
    public Integer getReportId() {
        return reportId;
    }

    public void setReportId(Integer reportId) {
        this.reportId = reportId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RegionsReportEntity that = (RegionsReportEntity) o;

        if (reportRowId != that.reportRowId) return false;
        if (regionName != null ? !regionName.equals(that.regionName) : that.regionName != null) return false;
        if (overallCount != null ? !overallCount.equals(that.overallCount) : that.overallCount != null) return false;
        if (overallVolume != null ? !overallVolume.equals(that.overallVolume) : that.overallVolume != null)
            return false;
        return !(averageCost != null ? !averageCost.equals(that.averageCost) : that.averageCost != null);

    }

    @Override
    public int hashCode() {
        int result = reportRowId;
        result = 31 * result + (regionName != null ? regionName.hashCode() : 0);
        result = 31 * result + (overallCount != null ? overallCount.hashCode() : 0);
        result = 31 * result + (overallVolume != null ? overallVolume.hashCode() : 0);
        result = 31 * result + (averageCost != null ? averageCost.hashCode() : 0);
        return result;
    }

    public List<String> toList() {
        List<String> list = new ArrayList<String>();
        list.add(regionName);
        list.add(overallCount.toString());
        list.add(overallVolume.toString());
        list.add(averageCost.toString());
        return list;
    }
}

