package com.vdsirotkin.delivery.db;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by ������� on 17.11.2015.
 */
@Entity
@Table(name = "REGIONS")
@NamedQueries({
        @NamedQuery(name = "findAllRegions", query = "from RegionsEntity "),
        @NamedQuery(name = "findRegionById", query = "from RegionsEntity where regionId=:regionId")
})
public class RegionsEntity implements Serializable{
    private int regionId;
    private String regionName;
    private Integer regionCost;

    @Id
    @Column(name = "REGION_ID")
    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    @Basic
    @Column(name = "REGION_NAME")
    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    @Basic
    @Column(name = "REGION_COST")
    public Integer getRegionCost() {
        return regionCost;
    }

    public void setRegionCost(Integer regionCost) {
        this.regionCost = regionCost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RegionsEntity that = (RegionsEntity) o;

        if (regionId != that.regionId) return false;
        if (regionName != null ? !regionName.equals(that.regionName) : that.regionName != null) return false;
        if (regionCost != null ? !regionCost.equals(that.regionCost) : that.regionCost != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = regionId;
        result = 31 * result + (regionName != null ? regionName.hashCode() : 0);
        result = 31 * result + (regionCost != null ? regionCost.hashCode() : 0);
        return result;
    }
}
