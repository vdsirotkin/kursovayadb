<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8" language="java" %>
<jsp:include page="top.jsp"/>
<c:forEach items="${orders}" var="order">
  Номер заказа: ${order} <a href="<c:url value="/print.ttn.form?orderId=${order}"/>" class="btn btn-success">Напечатать</a><br/>
</c:forEach>
<jsp:include page="bottom.jsp"/>