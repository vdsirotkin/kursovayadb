<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8" language="java" %>
<jsp:include page="top.jsp"/>
<table style="width: 100%"><tr><td align="left"><h4 style="text-align: left">Список заказов</h4></td><td align="right" valign="bottom"><a href="<c:url value="/edit.orders.form?orderId=-1" />" class="btn btn-warning">Добавить новый заказ</a></td></tr></table>
<div align="right" style="display: inline-block;"></div>
<%--<br/>--%>
<jsp:useBean id="order" class="com.vdsirotkin.delivery.db.DeliveryOrderEntity"/>
<div class="table-view" style="padding-top: 10px">
    <table class="table table-striped table-bordered">
        <tbody>
        <tr>
            <th style="vertical-align: middle; text-align: center;">Номер заказа</th>
            <th>Дата заказа</th>
            <th>Имя заказчика</th>
            <th>Телефон заказчика</th>
            <th>Регион</th>
            <th>Номер счета</th>
            <th>&nbsp;</th>
        </tr>
        <c:if test="${not empty orders}">
            <c:forEach items="${orders}" var="order">
                <tr>
                    <td style="vertical-align: middle; text-align: center;">${order.orderId}</td>
                    <fmt:formatDate value="${order.orderDate}" type="date" var="orderDate2" pattern="yyyy-MM-dd"/>
                    <td style="vertical-align: middle" nowrap>${orderDate2}</td>
                    <td style="vertical-align: middle" nowrap>${order.orderName}</td>
                    <td style="vertical-align: middle">${order.orderTelephone}</td>
                    <td style="vertical-align: middle" >${order.region.regionName}</td>
                    <td style="vertical-align: middle">${order.listBills.toArray()[0].billNumber}</td>
                    <td style="vertical-align: middle; text-align: center" nowrap>
                        <div class="dropdown">
                            <a href="<c:url value="/edit.orders.form?orderId=${order.orderId}" />" class="btn btn-success"><span class="glyphicon glyphicon-pencil"></span> Изменить</a>
                            <a href="<c:url value="/done.orders.form?orderId=${order.orderId}" />" class="btn btn-primary"><span class="glyphicon glyphicon-check"></span>Сделано</a>
                            <a href="<c:url value="/remove.orders.form?orderId=${order.orderId}" />" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span>Удалить</a>
                        </div>
                    </td>
                </tr>
            </c:forEach>
        </c:if>
        </tbody>
    </table>
</div>
<jsp:include page="bottom.jsp"/>