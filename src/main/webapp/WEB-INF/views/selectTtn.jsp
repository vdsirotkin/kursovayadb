<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8" language="java" %>
<jsp:include page="top.jsp"/>
<form action="<c:url value="/list.ttn.form"/>" method="get">
  <div id="datetime">
    <select name="carId" class="selectpicker">
      <c:forEach items="${cars}" var="car">
        <option value="${car.carId}">Машина №${car.carId}</option>
      </c:forEach>
    </select>
    <select name="time" class="selectpicker">
        <option value="11:00">11:00</option>
        <option value="13:00">13:00</option>
        <option value="15:00">15:00</option>
        <option value="17:00">17:00</option>
    </select>
    <input type="submit" value="Получить список ТТН" class="btn btn-success">
  </div>
</form>
<jsp:include page="bottom.jsp"/>