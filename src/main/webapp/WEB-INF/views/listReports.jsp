<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8" language="java" %>
<jsp:include page="top.jsp"/>
<div class="table-view">
    <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_REPORT_EDIT')">
        <div align="right"><a href="<c:url value="/create.report.form" />" class="btn btn-success"> Добавить новый отчет</a></div>
    </sec:authorize>
    <table class="table-stripped" style="border-spacing: 10px; border-collapse: separate;">
        <tr>
            <%--<th height="34px">Название отчета</th>--%>
            <th>Тип отчета</th>
            <th>Дата начала отчета</th>
            <th>Дата окончания отчета</th>
            <th>&nbsp;</th>
        </tr>
        <c:forEach items="${listReports}" var="report">
        <tr>
            <td width="150px">${report.reportName}</td>
            <td width="150px">${report.reportTypeString}</td>
            <fmt:formatDate value="${report.reportStartDate}" pattern="yyyy-MM-dd" var="reportStartDate"/>
            <fmt:formatDate value="${report.reportEndDate}" pattern="yyyy-MM-dd" var="reportEndDate"/>
            <td width="150px">${reportStartDate}</td>
            <td width="150px">${reportEndDate}</td>
            <td><a href="<c:url value="/show.report.form?reportId=${report.reportId}&reportType=${report.reportType}" />" class="btn btn-success">Просмотр</a>
                <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_REPORT_EDIT')">
                    <a href="<c:url value="/delete.report.form?reportId=${report.reportId}" />" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span>Удалить</a></td>
                </sec:authorize>
        </tr>
        </c:forEach>
    </table>
</div>
<jsp:include page="bottom.jsp"/>