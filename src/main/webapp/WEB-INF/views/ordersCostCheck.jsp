<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<jsp:include page="top.jsp"/>
<c:if test="${not empty cost}">
    Цена: ${cost}. Сохранить?<br/>
</c:if>
<c:if test="${not empty message}">
    <b>${message}</b>
</c:if>
<form action="<c:url value="/edit.orders.form"/>" method="post" accept-charset="UTF-8">
    <input type="hidden" value="true" name="checked">
    <input type="hidden" name="orderId" value="${order.orderId}"/>
    <fmt:formatDate type="date" value="${order.orderDate}" var="orderDate2" pattern="yyyy-MM-dd" />
    <input name="orderDate" type="hidden" value="${orderDate2}" class="form-control">
    <input name="orderName" type="hidden" value="${order.orderName}" class="form-control">
    <input name="orderTelephone" type="hidden" value="${order.orderTelephone}" class="form-control">
    <input name="regionId" type="hidden" value="${order.regionId}" class="form-control">
    <c:forEach items="${billIds}" var="billId">
        <input type="hidden" name="billIds" value="${billId}">
    </c:forEach>
    <c:if test="${not empty cost}">
        <input type="submit" class="btn btn-success" value="Сохранить">
    </c:if>
    <input type="button" class="btn btn-danger" value="Назад" onclick="history.back()">
</form>

<jsp:include page="bottom.jsp"/>