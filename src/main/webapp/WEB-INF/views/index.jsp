<%@ page import="com.vdsirotkin.delivery.db.DeliveryOrderEntity" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sec"
          uri="http://www.springframework.org/security/tags"%>
<%@ page contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8" language="java" %>
<jsp:include page="top.jsp"/>
    <b>Меню:</b>
    <div style="padding-top: 10px">
        <ul class="list-group">
            <sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')">
                <li class="list-group-item"><a class="list-group-item-text" href="<c:url value="/list.orders.form"/>">Просмотр списка заказов</a></li>
            </sec:authorize>
            <sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_LOGISTIC')">
                <li class="list-group-item"><a class="list-group-item-text" href="<c:url value="/list.carsorders.form"/>">Редактирование доставок</a></li>
            </sec:authorize>
            <sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_REPORT_EDIT')">
                <li class="list-group-item"><a class="list-group-item-text" href="<c:url value="/list.reports.form"/>">Просмотр и изменение отчеты</a></li>
            </sec:authorize>
            <sec:authorize access="hasRole('ROLE_REPORT_VIEW')">
                <li class="list-group-item"><a class="list-group-item-text" href="<c:url value="/list.reports.form"/>">Просматривать отчеты</a></li>
            </sec:authorize>
        </ul>
    </div>
<jsp:include page="bottom.jsp"/>