<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="top.jsp"/>
<%@ page contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8" language="java" %>
<form action="create.report.form" method="post">
    <table class="table table-stripped">
        <%--<tr>--%>
            <%--<td>Название отчета</td>--%>
            <%--<td><input type="text" name="reportName" class="form-control" value="<c:if test="${not empty report}">${report.reportName}</c:if>"></td>--%>
        <%--</tr>--%>
        <tr>
            <td>Тип отчета</td>
            <td><input type="radio" name="reportType" value="1"<c:if test="${not empty report && report.reportType == 1}"><c:out value="checked"/></c:if>>Отчет об активности регионов<br/>
                <input type="radio" name="reportType" value="2"<c:if test="${not empty report && report.reportType == 2}"><c:out value="checked"/></c:if>>Отчет об активности машин</td>
        </tr>
        <tr>
            <td>Дата начала</td>
            <td id="datetime1"><input name="reportStartDate" type="text" class="form-control" value="<c:if test="${not empty report}">${report.reportStartDate}</c:if>"></td>
        </tr>
        <tr>
            <td>Дата окончания</td>
            <td id="datetime2"><input name="reportEndDate" type="text" class="form-control" value="<c:if test="${not empty report}">${report.reportEndDate}</c:if>"></td>
        </tr>
    </table>
    <c:if test="${not empty message}">
        <h5>${message}</h5>
    </c:if>
    <input type="submit" class="btn btn-success" value="Сохранить"/>
    <c:if test="${not empty message}">
        <a href="<c:url value="/list.reports.form"/>" class="btn btn-danger">Назад</a>
    </c:if>
</form>
<script>
    $('#datetime1').find('input').datepicker({
        format:'yyyy-mm-dd',
        orientation: 'left bottom'
    });
    $('#datetime2').find('input').datepicker({
        format:'yyyy-mm-dd',
        orientation: 'left bottom'
    });
</script>
<jsp:include page="bottom.jsp"/>