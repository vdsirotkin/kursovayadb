<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8" language="java" %>
<jsp:include page="top.jsp"/>
<form action="<c:url value="/costcheck.orders.form" />" method="get">
    <b>Выберите товары</b><br/>
    <c:choose>
        <c:when test="${not empty message}">
            <b>${message}</b>
        </c:when>
        <c:otherwise>
            <c:if test="${not empty order}">
                <input type="hidden" name="orderId" value="${order.orderId}"/>
                <fmt:formatDate type="date" value="${order.orderDate}" var="orderDate2" pattern="yyyy-MM-dd" />
                <input name="orderDate" type="hidden" value="${orderDate2}" class="form-control">
                <input name="orderName" type="hidden" value="${order.orderName}" class="form-control">
                <input name="orderTelephone" type="hidden" value="${order.orderTelephone}" class="form-control">
                <input name="regionId" type="hidden" value="${order.regionId}" class="form-control">
            </c:if>
            <c:if test="${not empty bills}">
                <c:forEach items="${bills}" var="bill">
                    <label>
                        <input type="checkbox" name="billIds" value="${bill.billId}"
                        <c:if test="${bill.orderId==order.orderId}">
                            <c:out value="checked"/>
                        </c:if>
                                > ${bill.good.goodName}
                    </label>
                    <br/>
                </c:forEach>
                <input type="submit" class="btn btn-success" value="Далее">
                <input type="button" class="btn btn-danger" value="Back" onclick="history.back()">
            </c:if>
            <c:if test="${empty bills}">
                <b>Чек уже исполнен!</b><br/>
            </c:if>
        </c:otherwise>
    </c:choose>
</form>
<jsp:include page="bottom.jsp"/>