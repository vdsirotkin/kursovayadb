<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8" language="java" %>
<jsp:include page="top.jsp"/>
<div class="table-view">
    <form action="<c:url value="/checklist.orders.form"/>" method="get">
        <table class="table table-stripped">
            <tbody>
            <c:if test="${not empty order}">
                <c:if test="${not empty order.orderId}">
                    <input type="hidden" name="orderId" value="${order.orderId}"/>
                </c:if>
                <tr>
                    <td>Дата заказа</td>
                    <td id="datetime"><input name="orderDate2" type="text" value="${order.orderDate}" class="form-control"></td>
                </tr>
                <tr>
                    <td>Имя заказчика</td>
                    <td><input name="orderName" type="text" value="${order.orderName}" class="form-control"></td>
                </tr>
                <tr>
                    <td>Телефон заказчика</td>
                    <td><input name="orderTelephone" type="text" value="${order.orderTelephone}" class="form-control"></td>
                </tr>
                <tr>
                    <td>Номер чека</td>
                    <td>
                       <input type="text" class="form-control" name="billNumber" value="${order.listBills.toArray()[0].billNumber}">
                    </td>
                </tr>
                <tr>
                    <td>Регион</td>
                    <td>
                        <div>
                            <select class="selectpicker" name="regionId">
                                <c:forEach items="${regions}" var="region">
                                    <option value="${region.regionId}" <c:if test="${order.regionId==region.regionId}">selected</c:if> >${region.regionName}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </td>
                </tr>
            </c:if>
            </tbody>
        </table>
        <input type="submit" class="btn btn-success" value="Далее"/>
        <input type="hidden" value="false" name="checked">
    </form>
    <script>
        $('#datetime').find('input').datepicker({
            format:'yyyy-mm-dd',
            startDate:'0d'
        });
    </script>
</div>
<jsp:include page="bottom.jsp"/>