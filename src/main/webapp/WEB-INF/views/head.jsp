<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="<c:url value="/css/bootstrap.css" />" rel="stylesheet"/>
<link href="<c:url value="/css/bootstrap-datepicker.css" />" rel="stylesheet"/>
<link href="<c:url value="/css/bootstrap-select.css" />" rel="stylesheet"/>
<link href="<c:url value="/css/theme1.css" />" rel="stylesheet"/>
<link href="<c:url value="/css/custom.css" />" rel="stylesheet"/>
<script src="<c:url value="/js/jquery-1.11.3.js" />"></script>
<script src="<c:url value="/js/bootstrap.js" />"></script>
<script src="<c:url value="/js/bootstrap-datepicker.js" />"></script>
<script src="<c:url value="/js/bootstrap-select.js" />"></script>


