<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8" language="java" %>
<jsp:include page="top.jsp"/>
<%--@elvariable id="regions" type="java.util.List"--%>
<%--@elvariable id="regionId" type="java.lang.Integer"--%>
<form action="<c:url value="/list.carsorders.form"/>" method="get">
    <select class="selectpicker" name="regionId">
        <c:forEach items="${regions}" var="region">
            <option value="${region.regionId}"
            <c:if test="${not empty regionId && regionId==region.regionId}">selected</c:if> >${region.regionName}</option>
        </c:forEach>
    </select>
    <input type="submit" class="btn btn-success" value="Выбрать регион"/>
</form>
<c:if test="${not empty regionId}">
<div class="table-view">
    <table class="table table-stripped table-bordered">
        <tbody>
        <tr>
            <th style="vertical-align: middle; text-align: center;">Номер заказа</th>
            <th>Дата заказа</th>
            <th>Имя заказчика</th>
            <th>Телефон заказчика</th>
            <th>Объем</th>
            <th>&nbsp;</th>
        </tr>

        <%--@elvariable id="orders" type="java.util.list"--%>
        <form action="<c:url value="/save.carorder.form"/>" method="post">
            <input type="hidden" value="${regionId}" name="regionId"/>
        <c:if test="${not empty orders}">
            <c:forEach items="${orders}" var="order">
            <tr>
                <c:forEach items="${carsDeliveries}" var="carDelivery">
                    <c:if test="${carDelivery.orderId == order.orderId}">
                        <c:set var="carDeliveryUsed" value="${carDelivery}"/>
                    </c:if>
                </c:forEach>
                <input type="hidden" value="${order.orderId}" name="orderId"/>
                <td style="vertical-align: middle; text-align: center;">${order.orderId}</td>
                <fmt:formatDate value="${order.orderDate}" type="date" var="orderDate2" pattern="yyyy-MM-dd"/>
                <td style="vertical-align: middle" nowrap>${orderDate2}</td>
                <td style="vertical-align: middle" nowrap>${order.orderName}</td>
                <td style="vertical-align: middle">${order.orderTelephone}</td>
                <c:set var="overallVolume" value="${0}"/>
                <c:forEach items="${order.listBills.toArray()}" var="bill">
                    <c:set var="overallVolume" value="${overallVolume + bill.goodCount*bill.good.goodVolume}"/>
                </c:forEach>
                <td style="vertical-align: middle">${overallVolume}</td>
                <td style="vertical-align: middle; text-align: center" nowrap>
                    <div class="dropdown">
                        <%--<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Goods--%>
                            <%--<span class="caret"></span></button>--%>
                        <%--<ul class="dropdown-menu">--%>
                            <%--<c:forEach items="${order.listBills.toArray()}" var="bill">--%>
                                <%--<li><span>${bill.good.goodName} (${bill.goodCount})</span></li>--%>
                            <%--</c:forEach>--%>
                        <%--</ul>--%>
                        <select class="selectpicker" name="carId">
                            <option value="-1">Выберите машину</option>
                            <c:forEach items="${cars}" var="car">
                                <option value="${car.carId}"
                                        <c:if test="${carDeliveryUsed.carId == car.carId}">selected</c:if>
                                        >Машина #${car.carId} (volume ${car.carVolume})</option>
                            </c:forEach>
                        </select>
                            <fmt:formatDate value="${carDeliveryUsed.deliveryTime}" pattern="HH:mm" var="time"/>
                        <select class="selectpicker" name="orderTime">
                            <option value="11:00" <c:if test="${time == '11:00'}">selected</c:if> >11:00</option>
                            <option value="13:00" <c:if test="${time == '13:00'}">selected</c:if> >13:00</option>
                            <option value="15:00" <c:if test="${time == '15:00'}">selected</c:if> >15:00</option>
                            <option value="17:00" <c:if test="${time == '17:00'}">selected</c:if> >17:00</option>
                        </select>
                    </div>
                </td>
            </tr>

            </c:forEach>
        </c:if>
    </table>
    <input type="submit" class="btn btn-success" value="Save"/>
    <c:if test="${not empty errors}">
        <br/>
        <c:forEach items="${errors}" var="error">
            <c:out value="Внимание! Машина №${error.carId} перегружена в ${error.timeStr}! Доступный объем: ${error.carVolume}, выбранный объем: ${error.takenVolume}"/>
            <br/>
        </c:forEach>
    </c:if>
    </form>
</div>
</c:if>
<jsp:include page="bottom.jsp"/>