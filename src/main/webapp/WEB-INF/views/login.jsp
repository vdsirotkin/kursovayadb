<!DOCTYPE html>
<html>
<head>
    <title>Вход в информационную систему</title>
</head>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8" language="java" %>
<jsp:include page="head.jsp"/>

<body style="padding-top: 20px">
<form id="form" action="<c:url value='/login.do'/>" method="POST">

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Пожалуйста войдите</h3>
                    </div>
                    <div class="panel-body">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="Логин" name="username" type="text">
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Пароль" name="password" type="password" value="">
                            </div>
                            <c:if test="${not empty param.err}">
                                <div><c:out value="Неправильно введен логин или пароль"/></div>
                                <%--<div><c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}"/></div>--%>
                            </c:if>
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="Войти">
                            <%--<c:if test="${not empty param.out}">--%>
                                <%--<div>You've logged out successfully.</div>--%>
                            <%--</c:if>--%>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>
</body>
</html>