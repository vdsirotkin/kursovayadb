<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <meta  charset="utf-8">
  <title>ТНН</title>
  <style>
    table  {
      width: 500px;
      background: white;
      color: black;
      border-spacing: 0px;
      border-collapse:collapse;
      height:100px;
      margin-bottom: 50px;
      margin-top: 50px;


    }
    td, th {
      text-align: center;
      background:white;
      padding: 3px; /* Поля вокруг текста */
      border: 2px solid black;
    }

  </style>
</head>
<body>
<table align="center">

  <tr>
    <th>Номер заказа</th>
    <th>Дата заказа</th>
    <th>Адрес</th>
  </tr>
  <tr>
    <td>${billNumber}</td>
    <fmt:formatDate value="${order.orderDate}" var="orderDate2" pattern="yyyy-MM-hh"/>
    <td>${orderDate2}</td>
    <td>${order.region.regionName}</td>
  </tr>

</table>

<table align="center">

  <tr>
    <th>#</th>
    <th>Товар</th>
    <th width="90">Количество</th>
  </tr>

  <c:set var="counter" value="${1}"/>
  <c:forEach items="${bills}" var="bill">
    <tr>
      <td>${counter}</td>
      <td>${bill.good.goodName}</td>
      <td>${bill.goodCount}</td>
    </tr>
    <c:set var="counter" value="${counter+1}"/>
  </c:forEach>

</table>
</body>
</html>