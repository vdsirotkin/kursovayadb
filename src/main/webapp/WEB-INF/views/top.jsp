<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8" language="java" %>
<jsp:include page="head.jsp"/>
<html>
<head>
    <title>Информационная система для доставки</title>
    <meta charset="UTF-8">
</head>

<div id="menu-sticky-wrapper" class="sticky-wrapper" style="height: 50px;">
    <nav class="navbar navbar-inverse navbar-fixed-top" id="menu" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="<c:url value="/"/>">Система управления доставками</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_MANAGER')">
                        <li id="listOrders"><a href="<c:url value="/list.orders.form"/>">Заказы</a></li>
                    </sec:authorize>
                    <sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_LOGISTIC')">
                        <li id="listCars"><a href="<c:url value="/list.carsorders.form"/>">Доставка</a></li>
                        <li id="listTtn"><a href="<c:url value="/select.ttn.form"/>">ТТН</a></li>
                    </sec:authorize>
                    <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_REPORT_EDIT', 'ROLE_REPORT_VIEW')">
                        <li id="listReports"><a href="<c:url value="/list.reports.form"/>">Отчеты</a></li>
                    </sec:authorize>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li id="logout"><a href="<c:url value="/logout"/>" class="active"><b>Выйти</b></a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<body style=" padding-top: 30px; background: url(<c:url value="/img/img.jpg"/>) no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;">
<div>
    <div class="container">
        <div class="jumbotron" >
<script>
    $(document).ready(function(){
        var locationUrl = window.location.pathname;
        if (locationUrl.toLowerCase().indexOf('.orders') != -1) {
            $('#listOrders').addClass('active');
        } else if (locationUrl.toLowerCase().indexOf('.report') != -1) {
            $('#listReports').addClass('active');
        } else if (locationUrl.toLowerCase().indexOf('.cars') != -1) {
            $('#listCars').addClass('active');
        } else if (locationUrl.toLowerCase().indexOf('.ttn') != -1) {
            $('#listTtn').addClass('active');
        }
    });
</script>