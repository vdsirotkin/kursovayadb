<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="top.jsp"/>
<%@ page contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8" language="java" %>
<table class="table table-stripped">
    <tr>
        <c:forEach items="${headers}" var="header_elem">
            <th>${header_elem}</th>
        </c:forEach>
    </tr>
    <c:forEach items="${reportContent}" var="reportRow">
        <tr>
            <c:forEach items="${reportRow}" var="reportCell">
                <td>${reportCell}</td>
            </c:forEach>
        </tr>
    </c:forEach>
</table>
<a href="<c:url value="/list.reports.form"/>" class="btn btn-danger">Назад</a>
<%--<input type="button" class="btn btn-danger" value="Back" onclick="history.back()">--%>
<jsp:include page="bottom.jsp"/>