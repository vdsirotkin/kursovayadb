-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: kursovaya
-- ------------------------------------------------------
-- Server version	5.7.10-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bill`
--

DROP TABLE IF EXISTS `bill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill` (
  `BILL_ID` int(11) NOT NULL AUTO_INCREMENT,
  `BILL_NUMBER` int(11) DEFAULT NULL,
  `GOOD_ID` int(11) DEFAULT NULL,
  `GOOD_COUNT` int(11) DEFAULT NULL,
  `BILL_STATUS` int(11) DEFAULT NULL,
  `ORDER_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`BILL_ID`),
  KEY `bill_number_ind` (`BILL_NUMBER`),
  KEY `ORDER_ID` (`ORDER_ID`),
  KEY `GOOD_ID` (`GOOD_ID`),
  CONSTRAINT `BILL_ibfk_1` FOREIGN KEY (`ORDER_ID`) REFERENCES `delivery_order` (`ORDER_ID`),
  CONSTRAINT `BILL_ibfk_2` FOREIGN KEY (`GOOD_ID`) REFERENCES `goods` (`GOOD_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill`
--

LOCK TABLES `bill` WRITE;
/*!40000 ALTER TABLE `bill` DISABLE KEYS */;
INSERT INTO `bill` VALUES (1,1001,1,2,0,8),(2,1001,2,4,0,NULL),(3,1001,3,6,0,8),(4,1002,15,1,0,NULL),(5,1002,4,3,0,NULL),(6,1003,5,2,0,3),(7,1004,13,5,0,NULL),(8,1004,4,2,0,NULL),(9,1005,8,3,0,27),(10,1006,10,4,0,NULL),(11,1007,9,3,0,30),(12,1007,7,6,0,30),(13,1008,12,1,0,23),(14,1009,6,3,1,21),(15,1009,14,5,1,21),(16,1009,2,3,1,22),(17,1010,8,2,0,24),(18,1011,11,6,0,28),(19,1011,4,2,0,28),(20,1011,9,6,0,NULL);
/*!40000 ALTER TABLE `bill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car_report`
--

DROP TABLE IF EXISTS `car_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car_report` (
  `CAR_REPORT_ROW_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CAR_ID` int(11) DEFAULT NULL,
  `REGION_NAME` varchar(45) DEFAULT NULL,
  `ORDERS_COUNT` int(11) DEFAULT NULL,
  `VOLUME` float DEFAULT NULL,
  `REPORT_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`CAR_REPORT_ROW_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car_report`
--

LOCK TABLES `car_report` WRITE;
/*!40000 ALTER TABLE `car_report` DISABLE KEYS */;
INSERT INTO `car_report` VALUES (46,1,'Центральный',1,88,21),(47,3,'Центральный',1,170,21),(48,2,'Северный',2,7.5,21),(49,4,'Северный',1,159,21),(50,5,'Южный',1,12,21),(51,1,'Центральный',1,88,22),(52,3,'Центральный',1,170,22),(53,2,'Северный',2,7.5,22),(54,4,'Северный',1,159,22),(55,5,'Южный',1,12,22);
/*!40000 ALTER TABLE `car_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cars`
--

DROP TABLE IF EXISTS `cars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cars` (
  `CAR_ID` int(11) NOT NULL,
  `CAR_REGION` int(11) DEFAULT NULL,
  `CAR_VOLUME` int(11) DEFAULT NULL,
  PRIMARY KEY (`CAR_ID`),
  KEY `cars_region_fk_idx` (`CAR_REGION`),
  CONSTRAINT `cars_region_fk` FOREIGN KEY (`CAR_REGION`) REFERENCES `regions` (`REGION_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cars`
--

LOCK TABLES `cars` WRITE;
/*!40000 ALTER TABLE `cars` DISABLE KEYS */;
INSERT INTO `cars` VALUES (1,1,150),(2,2,200),(3,1,350),(4,2,250),(5,6,100);
/*!40000 ALTER TABLE `cars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cars_delivery`
--

DROP TABLE IF EXISTS `cars_delivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cars_delivery` (
  `CAR_DELIVERY_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CAR_ID` int(11) DEFAULT NULL,
  `ORDER_ID` int(11) DEFAULT NULL,
  `DELIVERY_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`CAR_DELIVERY_ID`),
  KEY `car_fk_idx` (`CAR_ID`),
  KEY `bill_fk_idx` (`ORDER_ID`),
  CONSTRAINT `car_fk` FOREIGN KEY (`CAR_ID`) REFERENCES `cars` (`CAR_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `order_fk` FOREIGN KEY (`ORDER_ID`) REFERENCES `delivery_order` (`ORDER_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cars_delivery`
--

LOCK TABLES `cars_delivery` WRITE;
/*!40000 ALTER TABLE `cars_delivery` DISABLE KEYS */;
INSERT INTO `cars_delivery` VALUES (1,3,8,'2015-12-22 11:00:00'),(3,2,23,'2015-12-15 11:00:00'),(4,2,24,'2015-12-15 13:00:00'),(6,5,27,'2015-12-15 15:00:00'),(8,1,30,'2015-12-22 13:00:00');
/*!40000 ALTER TABLE `cars_delivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `delivery_order`
--

DROP TABLE IF EXISTS `delivery_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delivery_order` (
  `ORDER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_DATE` datetime DEFAULT NULL,
  `ORDER_NAME` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `ORDER_STATUS` int(11) DEFAULT '0',
  `ORDER_TELEPHONE` varchar(100) DEFAULT NULL,
  `REGION_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ORDER_ID`),
  KEY `region_id_del` (`REGION_ID`),
  CONSTRAINT `region_id_del` FOREIGN KEY (`REGION_ID`) REFERENCES `regions` (`REGION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delivery_order`
--

LOCK TABLES `delivery_order` WRITE;
/*!40000 ALTER TABLE `delivery_order` DISABLE KEYS */;
INSERT INTO `delivery_order` VALUES (3,'2015-06-03 00:00:00','Test3',0,'+7543219876',3),(8,'2015-12-07 00:00:00','Виталий Сироткин',0,'+7925869143',1),(21,'2015-12-14 00:00:00','Чернопазова Александра',1,'+7925869143',1),(22,'2015-12-14 00:00:00','Чернопазова Александра',1,'+71234567981',1),(23,'2015-12-14 00:00:00','Чернопазова Александра',0,'+7925869143',2),(24,'2015-12-13 00:00:00','Чернопазова Александра',0,'+7925869143',2),(27,'2015-12-15 00:00:00','Станислав Пересыпко',0,'+7666666699',6),(28,'2015-12-16 00:00:00','Сироткин Виталий Дмитриевич',0,'+7925869143',4),(30,'2015-12-22 00:00:00','Собин Александр Владимирович',0,'+7925869143',1);
/*!40000 ALTER TABLE `delivery_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `goods`
--

DROP TABLE IF EXISTS `goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goods` (
  `GOOD_ID` int(11) NOT NULL AUTO_INCREMENT,
  `GOOD_NAME` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `GOOD_VOLUME` int(11) DEFAULT NULL,
  PRIMARY KEY (`GOOD_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goods`
--

LOCK TABLES `goods` WRITE;
/*!40000 ALTER TABLE `goods` DISABLE KEYS */;
INSERT INTO `goods` VALUES (1,'Стул',10),(2,'Диван',15),(3,'Кресло',25),(4,'Шкаф',9),(5,'Стеллаж',14),(6,'Кровать',6),(7,'Полка',19),(8,'Комод',4),(9,'Стол',15),(10,'Табуретка',13),(11,'Тумба',26),(12,'Зеркало',7),(13,'Матрас',14),(14,'Пуф',6),(15,'Тахта',16);
/*!40000 ALTER TABLE `goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `regions`
--

DROP TABLE IF EXISTS `regions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regions` (
  `REGION_ID` int(11) NOT NULL,
  `REGION_NAME` varchar(200) DEFAULT NULL,
  `REGION_COST` int(11) DEFAULT NULL,
  PRIMARY KEY (`REGION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `regions`
--

LOCK TABLES `regions` WRITE;
/*!40000 ALTER TABLE `regions` DISABLE KEYS */;
INSERT INTO `regions` VALUES (1,'Центральный',100),(2,'Северный',200),(3,'Северо-западный',300),(4,'Западный',400),(5,'Юго-западный',500),(6,'Южный',600),(7,'Юго-восточный',700),(8,'Восточный',800),(9,'Северо-восточный',900);
/*!40000 ALTER TABLE `regions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `regions_report`
--

DROP TABLE IF EXISTS `regions_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regions_report` (
  `REPORT_ROW_ID` int(11) NOT NULL AUTO_INCREMENT,
  `REGION_NAME` varchar(100) DEFAULT NULL,
  `OVERALL_COUNT` int(11) DEFAULT NULL,
  `OVERALL_VOLUME` int(11) DEFAULT NULL,
  `AVERAGE_COST` float DEFAULT NULL,
  `REPORT_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`REPORT_ROW_ID`),
  KEY `report_fk_idx` (`REPORT_ID`),
  CONSTRAINT `report_fk` FOREIGN KEY (`REPORT_ID`) REFERENCES `reports` (`REPORT_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `regions_report`
--

LOCK TABLES `regions_report` WRITE;
/*!40000 ALTER TABLE `regions_report` DISABLE KEYS */;
INSERT INTO `regions_report` VALUES (40,'Center',14,340,122.667,19),(41,'South-East',32,1280,1780,19);
/*!40000 ALTER TABLE `regions_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reports`
--

DROP TABLE IF EXISTS `reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reports` (
  `REPORT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `REPORT_NAME` varchar(45) DEFAULT NULL,
  `REPORT_TYPE` int(11) DEFAULT NULL,
  `REPORT_START_DATE` datetime DEFAULT NULL,
  `REPORT_END_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`REPORT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reports`
--

LOCK TABLES `reports` WRITE;
/*!40000 ALTER TABLE `reports` DISABLE KEYS */;
INSERT INTO `reports` VALUES (19,'123',1,'2015-12-01 00:00:00','2015-12-10 00:00:00'),(22,'123',2,'2015-12-01 00:00:00','2015-12-31 00:00:00');
/*!40000 ALTER TABLE `reports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `ROLE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_NAME` varchar(100) DEFAULT NULL,
  `ROLE_NAME` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,'user1','ROLE_ADMIN'),(2,'user2','ROLE_MANAGER'),(3,'user3','ROLE_LOGISTIC'),(4,'report_view','ROLE_REPORT_VIEW'),(5,'report_edit','ROLE_REPORT_EDIT');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `USER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_NAME` varchar(100) DEFAULT NULL,
  `USER_PASSWORD` varchar(200) DEFAULT NULL,
  `USER_ENABLED` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'user1','b3daa77b4c04a9551b8781d03191fe098f325e67',1),(2,'user2','a1881c06eec96db9901c7bbfe41c42a3f08e9cb4',1),(3,'user3','0b7f849446d3383546d15a480966084442cd2193',1),(4,'report_edit','a27297bde9732f2e73fbc06db2611764e3ad9855',1),(5,'report_view','a27297bde9732f2e73fbc06db2611764e3ad9855',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'kursovaya'
--
/*!50003 DROP PROCEDURE IF EXISTS `CREATE_REGION_REPORT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `CREATE_REGION_REPORT`(in startDate datetime, in endDate datetime, in reportId int)
BEGIN
	declare _name varchar(200);
    declare overall, volume int;
    declare cost float;
    declare done boolean;
    declare report_data cursor for
      SELECT r.REGION_NAME as _name, sum(b.GOOD_COUNT) as overall, sum(g.GOOD_VOLUME) as volume, avg(g.GOOD_VOLUME*b.GOOD_COUNT*(r.REGION_COST/500)+r.REGION_COST) as cost	 FROM BILL b
        join DELIVERY_ORDER d on b.ORDER_ID=d.ORDER_ID
        join GOODS g on g.GOOD_ID=b.GOOD_ID
        join REGIONS r on r.REGION_ID=d.REGION_ID
      where d.ORDER_DATE between startDate and endDate
      group by d.REGION_ID;

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    open report_data;
    read_loop: LOOP
      fetch report_data into _name, overall, volume, cost;
      IF done THEN
        CLOSE report_data;
        LEAVE read_loop;
      END IF;
      insert into REGIONS_REPORT (REGION_NAME, OVERALL_COUNT, OVERALL_VOLUME, AVERAGE_COST, REPORT_ID) values (convert(_name using utf8), overall, volume, cost, reportId);

    end loop;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `SP_CREATE_CAR_REPORT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_CREATE_CAR_REPORT`(in start_date datetime, in end_date datetime, in reportId int)
BEGIN		
	declare _name varchar(200);
    declare carId, orderCount int;
    declare _volume float;
    declare done boolean;
    declare report_data cursor for
		select CAR_ID, REGION_NAME, count(ORDER_ID), avg(volumeAvg) from
			(select cd.CAR_ID, c.CAR_REGION, r.REGION_NAME, cd.DELIVERY_TIME, cd.ORDER_ID, sum(b.GOOD_COUNT*g.GOOD_VOLUME) as volumeAvg from CARS_DELIVERY cd
				join bill b on b.ORDER_ID=cd.ORDER_ID
				join goods g on g.GOOD_ID=b.GOOD_ID
				join cars c on c.CAR_ID=cd.CAR_ID
				join regions r on r.REGION_ID=c.CAR_REGION
				group by cd.ORDER_ID) c
			where c.DELIVERY_TIME between start_date and end_date
			group by c.CAR_ID
			order by c.CAR_REGION;
            
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    open report_data;
    read_loop: LOOP
		fetch report_data into carId, _name, orderCount, _volume;
        select carId, convert(_name using utf8), orderCount, _volume;
		IF done THEN
			CLOSE report_data;
			LEAVE read_loop;
		END IF;
		INSERT INTO `kursach`.`car_report` (`CAR_ID`,`REGION_NAME`,`ORDERS_COUNT`,`VOLUME`,`REPORT_ID`)
			VALUES (carId,convert(_name using utf8),orderCount,_volume,reportId);
    end loop;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-21 23:10:06
